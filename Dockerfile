FROM ruby:latest

COPY . .

ENTRYPOINT ["ruby", "check_price.rb"]
