#!/bin/bash

date=`date | awk '{ printf "%s %s %s", $1, $2, $3 }'`
email='calvinlmc@gmail.com'
url='https://www.ipdusa.com/products/4920/112327-sport-lowering-springs-240-wagon'
# url='https://www.ipdusa.com/products/11173/124590-power-steering-pump-upgrade-w-metal-pulley-2000-2004-c70-s70-s60-v70-xc70-s80'
price=$(ruby check_price.rb $url)
price_rc=$?

if [ $price_rc -eq 0 ]; then
  echo -e $price | mail -s "IPD Sale!!! $date" -a "From: root@calvinm.io" $email
fi
