#!/usr/bin/ruby

require 'json'
require 'net/http'
require 'net/smtp'

def main
  # 'https://www.ipdusa.com/products/11173/124590-power-steering-pump-upgrade-w-metal-pulley-2000-2004-c70-s70-s60-v70-xc70-s80'
  uri = URI.parse(ARGV[0])
  html = Net::HTTP.get_response(uri)
  html.body =~ /<script type="application\/ld\+json">\r\n(?<json>.*)\r\n<\/script>/
  json = JSON.parse($~['json'])
  name = json['name']
  price = json['offers'].map { |o| o['price'].to_f }.max
  sale_price = json['offers'].map { |o| o['salePrice'].to_f if o['salePrice'].to_f > 0 }.min
  puts "Product: #{name}"
  puts "Regular price: $#{price}"
  if sale_price
    puts "Sale price $#{sale_price}"
    puts "Savings of $#{'%.02f' % (price - sale_price)}"
  else
    puts "No sale price"
  end
  exit 0
end

main unless $0 == "irb"
